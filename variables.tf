variable "region" {
  description = "this gives region description"
  type = "string"
}

variable "ami" {
  description = "this is for ami"
  type = "string"
}

variable "instance_type" {
  description = "this is for instance type"
  type = "string"
}

variable "subnet" {
  description = "this is for subnet"
  type = "string"
}

variable "security_group" {
  description = "this is for describe security groups"
  type = "string"
}
