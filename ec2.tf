provider "aws" {
 profile = "default"
 region = "${var.region}"
}

resource "aws_instance" "myec2" {
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  subnet_id = "${var.subnet}"
  security_groups = ["${var.security_group}"]
}